var fs = require('fs'),
    path = require('path'),
    io = require('socket.io-client'),
    serial,
    com = require("serialport"),
    SerialPort = com.SerialPort,
    portName = '/dev/ttyAMA0',
    receivedData = "",
    mac = null,
    temperature = null,
    settings = { cpuTimeout: 5000 }, //5 seconds
    socket,
    cpuDaemon;

var exec = require('child_process').exec;
var child;
var stream = true;
var buffer = {};
var dup = "";

var syncTimeout = 5000;
var syncDaemon = null;

function sync() {
    syncDaemon = setTimeout(sync, syncTimeout);
if (stream)    
socket.emit('sync', new Date().getTime(), buffer, mac);
    console.log("synced");
}

function connect3g() {
    var command = 'sudo /usr/bin/modem3g/sakis3g connect APN="gprs.base.be" SIM_PIN="1111"';
    // executes `pwd`
    child = exec(command, function (error, stdout, stderr) {
        //sys.print('stdout: ' + stdout);
        //sys.print('stderr: ' + stderr);
        if (error !== null) {
            console.log('exec error: ' + error);
        } else {
            console.log(stdout.trim());
        }
    });
}

function getMACAddresses() {
    var macs = {}
    var devs = fs.readdirSync('/sys/class/net/');
    devs.forEach(function (dev) {
        var fn = path.join('/sys/class/net', dev, 'address');
        if (dev.substr(0, 3) == 'eth' && fs.existsSync(fn)) {
            macs[dev] = fs.readFileSync(fn).toString().trim();
            mac = fs.readFileSync(fn).toString().trim();
        }
    });
    //buffer['mac'] = mac;
}
function parseData(data) {
    if (data != dup && data != "" && data != " ") {
//        console.log(data);
        try {
            var json = JSON.parse(data);
            if (syncDaemon == null)
                sync();
            //console.log(json);
            for (var key in json) {
                //console.log(key + " is " + json[key]);
                buffer[key] = json[key];
            }
        } catch (e) {
            console.log(data);
            console.log("not valid");
        }
        dup = data;
    }
}
// Listen to serial port
function serialListener() {
    serial = new SerialPort(portName, {
        baudrate: 115200,
        buffersize: 512,
        parser: com.parsers.readline('\r\n')

    });
    serial.on('open', function () {
        console.log('Port open');
    });
    serial.on('data', function (data) {
        parseData(data);
    });
}

function isInteger(n) {
    //return true;
    return n === +n && n === (n | 0);
}

function getCpuTemperature() {
    if (isInteger(settings.cpuTimeout))
        cpuDaemon = setTimeout(function () { getCpuTemperature(); }, settings.cpuTimeout);
    // executes `pwd`
    child = exec("/opt/vc/bin/vcgencmd measure_temp | cut -c6-9", function (error, stdout, stderr) {
        //sys.print('stdout: ' + stdout);
        //sys.print('stderr: ' + stderr);
        if (error !== null) {
            console.log('exec error: ' + error);
        } else {
            temperature = stdout.trim();
            buffer['cpuTemperature'] = temperature;
            console.log(temperature);
            socket.emit('temperatureUpdate pi', new Date().getTime(), temperature);
        }
    });
}

function listenForSocket() {


var cmd = 'control-' + mac.replace(/:/g, '');
socket.on(cmd, function(data) {
console.log("Command received: " + data);

switch (data) {
case 1:
stream = true;
break;
case 2:
stream = false;
break;
}

serial.write("" + data);
console.log("written to serial");
});


    // Add a connect listener
    socket.on('connect', function (socket) {
        console.log('Connection with server!');
    });
    socket.on('command host', function (data) {
        //  console.log(data);
        console.log(data);
        console.log(mac);
        console.log(data.command);

        if (data.target == mac)
            serial.write(data.command);
    });
}

function initSocket() {
    // Connect to server
    socket = io.connect('http://188.166.33.33', {
        reconnect: true
    });
    //Broadcast connection with mac address
    socket.emit('iot-connect', mac);
    listenForSocket();
}
function init() {
    connect3g();
    getMACAddresses();
    getCpuTemperature();
    console.log(mac + ' initiated');
    serialListener();
    initSocket();
    //sync();
}
init();
